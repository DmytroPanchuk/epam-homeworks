const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Nov', 'Dec'];
const toYears = 31557600000;
const toDays = 86400000;

let getAge = (birthday) => {
    return Math.floor((new Date() - birthday) / toYears)
};

let getWeekday = (date) => {
    let Day = new Date(date);
    return days[Day.getDay()]
};

let getAmountDaysToNewYear = (date) => {
    let convert = new Date(date);
    let year = convert.getFullYear() + 1;
    return Math.floor((new Date(year, 0) - date) / toDays)
};

let getProgrammersDay = (year) => {
    let amountOfDays = 256;
    let programmerDay = new Date(year, 0, amountOfDays);
    console.log(programmerDay.getMonth());
    return `${programmerDay.getDate()} ${month[programmerDay.getMonth()]}, ${programmerDay.getFullYear()
        } (${getWeekday(programmerDay)})`
    
};

let howFarIs = (weekday) => {
    let today = new Date().getDay();
    let index = 0;
    for (let i = 0; i < days.length; i++) {
        if (days[i] === weekday) {  
            index = i;
        }
    }
    if (index > today) {
        return `It's ${index - today} day(s) left till ${weekday}`
    } else if (index === today) {
        return `Hey, today is ${weekday} =)`
    } else {
        return `It's ${days.length - today + index} day(s) left till ${weekday}`
    }
};

let isValidIdentifier = (str) => {
    let regexp = /^[a-zA-Z_$][a-zA-Z_$0-9]*$/
    return regexp.test(str);
};

let capitalize = (str) => {
    let regexp = /(\b[a-z](?!\s))/g;
    return str.replace(regexp, (a) => a.toUpperCase());
};

let isValidAudioFile = (audio) => {
    let regexp = /[a-z][.aac|.mp3|.flac|.alac]*$/gi;
    return regexp.test(audio);
};

let getHexadecimalColor = (color) => {
    let regexp = /#[0-9a-f]{6}|#[0-9a-f]{3}/gi;
    return color.match(regexp)
};

let isValidPassword = (password) => {
    let regexp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/;
    return regexp.test(password)
};

let addThousandsSeparators = (value) => {
    let regexp = /\B(?=(\d{3})+(?!\d))/g;
    return value.toString().replace(regexp, ',')
};

let getAllUrlsFromText = (text) => {
    const regex =
    /(https:\/\/.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/g;
    return text.match(regex);
};
