const appRoot = document.getElementById('app-root');

appRoot.insertAdjacentHTML('afterbegin', `
<h1 class="heading">Countries Search</h1>
<form>
<div class="choose">
<label for="choose">Please choose type of search: </label>
<div class="buttons">
<label for="regionInput">
<input type="radio" id="regionInput" name="radio" class="radio_input">
By Region</label>
<label for="languageInput">
<input type="radio" id="languageInput" name="radio" class="radio_input">
By Language</label>
</div>
</div>
<label for="select">Please choose search query:</label>
<select class="selection" id="select" disabled>
<option disabled selected>Select value</option>
</select>
</form>
<table id="table" class="content_table"></table>`)

let rgInput = document.getElementById('regionInput');
let langInput = document.getElementById('languageInput');
let select = document.getElementById('select');
let table = document.getElementById('table');

const RegionsList = externalService.getRegionsList();
const LanguagesList = externalService.getLanguagesList();

let renderTable = function (arr) {
    table.innerHTML = `
    <th>Country name
    <span class="span" id="span">&#8593;</span>
    </th>
    <th>Capital</th>
    <th>World Region</th>
    <th>Languages</th>
    <th>Area
    <span class="span" id="arrow">&#8593;</span>
    </th>
    <th>Flag</th>
    `
    for (let el of arr) {
        let str = '';
        for (let lang in el.languages) {
            if (el.languages) {
                str += el.languages[lang] + ' ';
            }
        }
        table.innerHTML += `
        <tr>
        <td>${el.name}</td>
        <td>${el.capital}</td>
        <td>${el.region}</td>
        <td>${str}</td>
        <td>${el.area}</td>
        <td><img src="${el.flagURL}"></td>
        </tr>
        `
    }
}

rgInput.addEventListener('change', function () {
    select.removeAttribute('disabled');
    select.length = 0;
    select.insertAdjacentHTML('afterbegin', `<option disabled selected>Select value</option>`);
    for (let i = 0; i < RegionsList.length; i++) {
        let option = document.createElement('option');
        option.innerHTML = RegionsList[i];
        select.appendChild(option)
    }
    table.innerHTML = '';
    table.innerHTML += `<p>No items, please choose search query...</p>`
    select.addEventListener('change', function () {
        renderTable(externalService.getCountryListByRegion(select.value));
    })
})

langInput.addEventListener('change', function () {
    select.removeAttribute('disabled');
    select.length = 0;
    select.insertAdjacentHTML('afterbegin', `<option disabled selected>Select value</option>`);
    for (let i = 0; i < LanguagesList.length; i++) {
        let option = document.createElement('option');
        option.innerHTML = LanguagesList[i];
        select.appendChild(option)
    }
    table.innerHTML = '';
    table.innerHTML += `<p>No items, please choose search query...</p>`
    select.addEventListener('change', function () {
        renderTable(externalService.getCountryListByLanguage(select.value));
    })
})









/*
write your code here

list of all regions
externalService.getRegionsList();
list of all languages
externalService.getLanguagesList();
get countries list by language
externalService.getCountryListByLanguage()
get countries list by region
externalService.getCountryListByRegion()
*/

