let tableD = document.querySelectorAll('.table_game td');
let special = document.getElementById('special');
let first = document.querySelectorAll('td:nth-child(1)');
let two = 2;
for (let i = 0; i < tableD.length; i++) {
    tableD[i].addEventListener('click', function () {   
        tableD[i].style.backgroundColor = 'yellow';
    })
    special.addEventListener('click', function () {
        if (tableD[i].style.backgroundColor !== 'blue') {
            tableD[i].style.backgroundColor = 'yellow';
        }
    })
    for (let j = 0; j < first.length; j++){
        first[j].addEventListener('click', function () {
            first[j].style.backgroundColor = 'blue';
            if (j === 0) {
                document.querySelector('tr:nth-child(1) td:nth-child(2)').style.backgroundColor !== 'yellow' ?
                    document.querySelector('tr:nth-child(1) td:nth-child(2)').style.backgroundColor = 'blue' :
                    document.querySelector('tr:nth-child(1) td:nth-child(2)').style.backgroundColor = 'yellow';
            document.querySelector('tr:nth-child(1) td:nth-child(3)').style.backgroundColor !== 'yellow' ?
                    document.querySelector('tr:nth-child(1) td:nth-child(3)').style.backgroundColor = 'blue' :
                    document.querySelector('tr:nth-child(1) td:nth-child(3)').style.backgroundColor = 'yellow'
            }
            if (j === 1) {
                document.querySelector('tr:nth-child(2) td:nth-child(2)').style.backgroundColor !== 'yellow' ?
                    document.querySelector('tr:nth-child(2) td:nth-child(2)').style.backgroundColor = 'blue' :
                    document.querySelector('tr:nth-child(2) td:nth-child(2)').style.backgroundColor = 'yellow';
            document.querySelector('tr:nth-child(2) td:nth-child(3)').style.backgroundColor !== 'yellow' ?
                    document.querySelector('tr:nth-child(2) td:nth-child(3)').style.backgroundColor = 'blue' :
                    document.querySelector('tr:nth-child(2) td:nth-child(3)').style.backgroundColor = 'yellow'
            }
            if (j === two) {
                document.querySelector('tr:nth-child(3) td:nth-child(2)').style.backgroundColor !== 'yellow' ?
                    document.querySelector('tr:nth-child(3) td:nth-child(2)').style.backgroundColor = 'blue' :
                    document.querySelector('tr:nth-child(3) td:nth-child(2)').style.backgroundColor = 'yellow';
            document.querySelector('tr:nth-child(3) td:nth-child(3)').style.backgroundColor !== 'yellow' ?
                    document.querySelector('tr:nth-child(3) td:nth-child(3)').style.backgroundColor = 'blue' :
                    document.querySelector('tr:nth-child(3) td:nth-child(3)').style.backgroundColor = 'yellow'
            }
        });
    }
}

const phone = document.getElementById('phone');
const send = document.getElementById('send');
const regExp = /^[+380]{4}[0-9]{9}/;
const message = document.querySelector('.message');
const msgText = document.querySelector('.message p');
phone.addEventListener('input', function (e) {
    let value = e.target.value;
    if (regExp.test(value) === false) {
        phone.style.borderColor = 'lightcoral';
        send.setAttribute('disabled', 'disabled');
        console.log(send);
        message.classList.remove('display');
    } else {
        phone.style.borderColor = 'black';
        send.removeAttribute('disabled');
        message.classList.add('display');
    }
})
send.addEventListener('click', function (evt) {
    evt.preventDefault();
    message.style.backgroundColor = 'green';
    message.style.borderColor = 'black'
    msgText.innerHTML = 'Data was successfully sent'
    message.classList.remove('display');
})


const ball = document.getElementById('ball');
const field = document.getElementById('field');
const scoreA = document.querySelector('.scoreA');
const scoreB = document.querySelector('.scoreB')
const goal = document.querySelector('.goal');
scoreA.textContent = 0;
scoreB.textContent = 0;

field.onclick = function (event) {
    let fieldCoords = this.getBoundingClientRect();
    let left = event.clientX - fieldCoords.left - field.clientLeft;
    ball.style.left = left + 'px';
    let top = event.clientY - fieldCoords.top - field.clientTop;
    ball.style.top = top + 'px'
}
  
let hoop1 = document.querySelector('.hoop1');
hoop1.addEventListener('click', function () {
    ++scoreB.textContent;
    goal.innerHTML = 'Team B Score!';
    goal.style.color = 'red';
})

let hoop2 = document.querySelector('.hoop2');
    hoop2.addEventListener('click', function () {
        ++scoreA.textContent;
        goal.innerHTML = 'Team A Score!';
    goal.style.color = 'blue';
})