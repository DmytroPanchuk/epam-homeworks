const display = document.querySelector('.display');
let greets = prompt('Input event name', 'meeting');
if (greets) {
    display.classList.remove('display');
}

let form = document.getElementById('form');
let inputName = document.getElementById('name');
let inputTime = document.getElementById('time');
let inputPlace = document.getElementById('place');
let btnConfirm = document.getElementById('confirm');
let pattern = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/;

form.addEventListener('submit', function (evt) {
    if (!inputName.value || !inputTime.value || !inputPlace.value) {
        evt.preventDefault();
        alert('Input all data')
    } else if (!pattern.test(inputTime.value)) {
        evt.preventDefault();
        alert('Enter  time  in  format  hh:mm');
    } else {
        console.log(inputName.value + ' has a ' + greets + ' today at ' + inputTime.value)
        evt.preventDefault();
    }

})

const conventer = document.getElementById('conventer');
let euroCourse = 33.46;
let dollarCourse = 27.44;
const valueToFixed = 2;

conventer.addEventListener('click', function () {
    let euro = prompt('Amount of euro');
    if (isNaN(euro) || euro <= 0) {
        alert('Invalid data')
        return
    }
    let dollar = prompt('Amount of dollars');
    if (isNaN(dollar) || dollar <= 0) {
        alert('Invalid data')
        return
    }
    let euro_UAH = (euro * euroCourse).toFixed(valueToFixed);
    let dollar_UAH = (dollar * dollarCourse).toFixed(valueToFixed);
    alert(euro + ' euros are equal ' + euro_UAH + 'hrvns, ' + dollar + ' dollars are equal ' + dollar_UAH + 'hrvns');
});

