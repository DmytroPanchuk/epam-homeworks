const two = 2;
const hundred = 100;
const thousand = 1000;


let amount = parseInt(prompt('Initial amount'));
if (isNaN(amount) || amount < thousand) {
    alert('Invalid input data')
}
let term = Math.round(parseInt(prompt('Number of years')));
if (isNaN(term) || term < 1) {
    alert('Invalid input data')
}
let percent = parseInt(prompt('Percentage of a year'));
if (isNaN(percent) || percent > hundred) {
    alert('Invalid input data')
}
let totalAmount = 0;
function calculateProfit(moneyAmount, yearsAmount, percentAmount) {
    for (let i = 0; i < yearsAmount; i++) {
        let earning = moneyAmount * percentAmount / hundred;
        totalAmount = earning + moneyAmount;
        moneyAmount+= earning;
    }
    totalAmount = totalAmount.toFixed(two);
    return totalAmount
}
calculateProfit(amount, term, percent)
let profit = (totalAmount - amount).toFixed(two);
if (isNaN(amount) || amount < thousand || isNaN(term) || term < 1 || isNaN(percent) || percent > hundred) {
    alert('Invalid input data');
} else {
    alert('Initial amount: ' + amount + '\n' +
    'Number of years: ' + term + '\n' +
    'Percentage of years: ' + percent + '\n' +
    '\n' + 'Total profit: ' + profit + '\n' +
    'Total amount: ' + totalAmount)
}