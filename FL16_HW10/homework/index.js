let isEqual = (firstNum, secondNum) => firstNum === secondNum;

let isBigger = (firstNum, secondNum) => firstNum > secondNum;

let storeNames = (...str) => [...str];

let getDifference = (firstNum, secondNum) => {
    if (firstNum > secondNum) {
        return firstNum - secondNum
    } else {
        return secondNum - firstNum
    }
}

let negativeCount = (arr) => {
    let sum = 0;
    arr.reduce((prev, current) => {
        if (current < 0) {
            sum++
        }
        return sum
    }, 0);
    return sum;
}

let letterCount = (strFirst, strSecond) => {
    let sum = 0;
    let arr = [...strFirst];
    arr.reduce((prev, current) => {
        if (current === strSecond) {
            sum++
            
        }
        return sum;
    }, 0);
    return sum;
};

let countPoints = (arr) => {
    const winPoints = 3;
    let points = 0;
    for (let el of arr) {
        let split = el.split(':');
        let x = parseInt(split[0]);
        let y = parseInt(split[1]);
        if (x > y) {
            points += winPoints;
        } else if (split[0] === split[1]) {
            points += 1;
        }
    }
    return points;
};