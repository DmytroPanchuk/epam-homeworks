function visitLink(path) {
	let counter = localStorage.getItem(path)
	localStorage[path] = ++counter;
}

function viewResults() {
	let pages = {
		Page1: localStorage.getItem('Page1'),
		Page2: localStorage.getItem('Page2'),
		Page3: localStorage.getItem('Page3')
	}
	for (let key in pages) {
		if (pages) {
			if (pages[key] === null) {
				pages[key] = 0;
			}
			const container = document.querySelector('.container');
			let ul = document.createElement('ul');
			container.appendChild(ul);
			let li = document.createElement('li');
			li.innerHTML = `You visited ${key} ${pages[key]} time(s)`
			ul.appendChild(li);
		}
	}

	localStorage.clear();
}


